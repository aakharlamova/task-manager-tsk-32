package ru.kharlamova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.api.repository.ITaskRepository;
import ru.kharlamova.tm.model.Task;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @NotNull
    public static Predicate<Task> predicateByProjectId(@NotNull final String projectId) {
        return s -> projectId.equals(s.getProjectId());
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entities.stream()
                .filter(predicateByUserId(userId))
                .filter(predicateByProjectId(projectId))
                .collect(Collectors.toList());
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        findAllByProjectId(userId, projectId).forEach(task -> entities.remove(task));
    }

    @NotNull
    @Override
    public Optional<Task> bindTaskByProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        @Nullable final Optional<Task> task = findById(userId, taskId);
        if (!task.isPresent()) return Optional.empty();
        task.get().setProjectId(projectId);
        return task;
    }

    @NotNull
    @Override
    public Optional<Task> unbindTaskFromProject(@NotNull final String userId, @NotNull final String taskId) {
       @Nullable final Optional<Task> task = findById(userId, taskId);
        if (!task.isPresent()) return Optional.empty();
        task.get().setProjectId(null);
        return task;
    }

}
