package ru.kharlamova.tm.repository;

import ru.kharlamova.tm.api.repository.IProjectRepository;
import ru.kharlamova.tm.model.Project;

public class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

}
