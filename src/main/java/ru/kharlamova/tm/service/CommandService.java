package ru.kharlamova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.api.repository.ICommandRepository;
import ru.kharlamova.tm.api.service.ICommandService;
import ru.kharlamova.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandService implements ICommandService {

    @NotNull private final ICommandRepository commandRepository;

    @NotNull
    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public void add(AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Override
    public @NotNull Collection<String> getListArgumentName() {
        return commandRepository.getCommandArgs();
    }

    @Override
    public @NotNull Collection<String> getListCommandName() {
        return commandRepository.getCommandNames();
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getArgsCommands() {
        return commandRepository.getArgsCommands();
    }

}
