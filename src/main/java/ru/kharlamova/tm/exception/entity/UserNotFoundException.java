package ru.kharlamova.tm.exception.entity;

import ru.kharlamova.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found.");
    }

}
