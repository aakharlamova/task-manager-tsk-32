package ru.kharlamova.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.kharlamova.tm.api.service.IPropertyService;
import ru.kharlamova.tm.bootstrap.Bootstrap;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private static final String BACKUP_SAVE = "backup-save";

    @NotNull
    private static final String BACKUP_LOAD = "backup-load";

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    public final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap, @NotNull final IPropertyService propertyService) {
        this.bootstrap = bootstrap;
        this.propertyService = propertyService;
    }

    public void init() {
        load();
        start();
    }

    public void start() {
        es.scheduleWithFixedDelay(this,0,propertyService.getBackupInterval(), TimeUnit.SECONDS);
    }

    public void run() {
        bootstrap.parseCommand(BACKUP_SAVE);
    }

    public void load() {
        bootstrap.parseCommand(BACKUP_LOAD);
    }

    public void stop() {
        es.shutdown();
    }

}
